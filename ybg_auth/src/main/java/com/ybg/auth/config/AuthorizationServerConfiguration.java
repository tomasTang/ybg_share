package com.ybg.auth.config;


import javax.sql.DataSource;

import com.ybg.auth.config.mybatis.YbgJdbcClientDetailsService;
import com.ybg.framework.core.MyRedisTokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;


/**
 * 认证授权服务端
 *
 * @author leftso
 *
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {


	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	RedisConnectionFactory connectionFactory;
	/**
	 * 连接池配置信息
	 */
	@Autowired
	private DataSource dataSource;

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.authenticationManager(this.authenticationManager);
		// endpoints.accessTokenConverter(accessTokenConverter());
		endpoints.tokenStore(tokenStore());

	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {

		oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()") // isAuthenticated():排除anonymous
																						// isFullyAuthenticated():排除anonymous以及remember-me
				.allowFormAuthenticationForClients(); // 允许表单认证

	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

		// 默认的密码策略使用你注册的，springboot1版本是用明文，和springboot2不一样，当前系统默认是MD5加密校验注册到springbeen中，所以
		// 这里也是md5
		ClientDetailsService clientDetailsService = new YbgJdbcClientDetailsService(dataSource);

		clients.withClientDetails(clientDetailsService);

	}

	/** 其实默认有一个 但是，需要支持刷新token 非必要 **/
	@Bean
	@Primary
	public DefaultTokenServices tokenServices() {
		DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(tokenStore());
		// 如果不配置此项 则没有RefreshToken 返回
		defaultTokenServices.setSupportRefreshToken(true);
		return defaultTokenServices;
	}

	/**
	 * token store
	 *
	 * @param
	 * @return
	 */
	@Bean
	@Primary
	public TokenStore tokenStore() {

		// TokenStore tokenStore = new JwtTokenStore(accessTokenConverter());
		TokenStore tokenStore = new MyRedisTokenStore(connectionFactory);
		return tokenStore;
	}
}
