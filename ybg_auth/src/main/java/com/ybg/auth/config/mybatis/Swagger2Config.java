package com.ybg.auth.config.mybatis;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.Sets;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @program: uplus-web
 *
 * @description: Swagger2配置
 *
 * @author: Don
 *
 * @create: 2018-05-23 16:41
 **/
@Configuration
public class Swagger2Config {


	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("授权中心").description("授权中心WEB-API文档")
				.termsOfServiceUrl("http://www.88ybg.com").version("1.0").build();
	}

	@Bean
	public Docket configSpringfoxDocketForAll() {
		return new Docket(DocumentationType.SWAGGER_2)

				.produces(Sets.newHashSet("application/json")).consumes(Sets.newHashSet("application/json"))
				.protocols(Sets.newHashSet("http", "https"/**/)).forCodeGeneration(true).select().paths(regex(".*"))
				// .apis(RequestHandlerSelectors.basePackage("*"))
				.build().apiInfo(apiInfo());
	}
}
