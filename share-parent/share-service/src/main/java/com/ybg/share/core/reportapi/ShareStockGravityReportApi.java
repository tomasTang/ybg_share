package com.ybg.share.core.reportapi;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.share.core.dbapi.entity.ShareStockGravity;
import com.ybg.share.core.dbapi.service.ShareStockGravityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 地心引力API
 *
 * @author deament
 */
@Component("shareStockGravityReportApi")
public class ShareStockGravityReportApi {
    @Autowired
    private ShareStockGravityService shareStockGravityService;

    /***
     * 个股根据地心引力线筛选
     * @param dataSource
     * @param table
     * @param params
     * @return
     */
    public List<ShareStockGravity> searchStock(String dataSource, String table, Map<String, String> params) {
        String type = params.get("type");
        //1,1.2+   2,1.1~1.2  3,1.0~1.1 4,0.9~1.0 5,0.8~0.9  6,0.8- 7 all
        if (type == null) {
            type = "7";
        }
        QueryWrapper<ShareStockGravity> condition = new QueryWrapper<>();
        switch (type) {
            case "1":
                condition.gt(ShareStockGravity.RATE,1.2);

                break;
            case "2":
                condition.ge(ShareStockGravity.RATE,1.1);
                condition.lt(ShareStockGravity.RATE,1.2);
                break;
            case "3":
                condition.ge(ShareStockGravity.RATE,1.0);
                condition.lt(ShareStockGravity.RATE,1.1);

                break;
            case "4":
                condition.ge(ShareStockGravity.RATE,0.9);
                condition.lt(ShareStockGravity.RATE,1.0);
                break;
            case "5":
                condition.ge(ShareStockGravity.RATE,0.8);
                condition.lt(ShareStockGravity.RATE,0.9);
                break;
            case "6":
                condition.lt(ShareStockGravity.RATE,0.8);
                break;
            case "7":
                break;
        }
        condition.orderByAsc(ShareStockGravity.RATE);
        return shareStockGravityService.list(condition);
    }

}
