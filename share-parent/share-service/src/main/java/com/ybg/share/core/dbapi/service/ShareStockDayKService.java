package com.ybg.share.core.dbapi.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.share.core.remoteapi.dto.StockDayKDTO;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.remoteapi.vo.StockDayKVO;

/**
 * <p>
 * 行情日k数据 服务类
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-21
 */
public interface ShareStockDayKService extends IService<ShareStockDayK> {
    /**
     * 保存数据（不覆盖旧数据）
     *
     * @param shareStockDayK
     * @return
     */
    boolean saveIgnore(ShareStockDayK shareStockDayK);

    /**
     * 获取股票最近一次拉取日期
     *
     * @param code
     * @return
     */
    ShareStockDayK getMaxDateByCode(String code, Long stockId);

    /**
     * 初始化表
     *
     * @return
     */
    int initTable();

    /**
     * 分页
     * @param dto 参数
     * @return 列表
     */
    Page<StockDayKVO> page(StockDayKDTO dto);
    /**
     * 获取股票最近一次日K数据
     *
     * @param code
     * @return
     */
    ShareStockDayK getLastDayKByCode(String code, Long id);
}
