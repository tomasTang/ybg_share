package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareStockGravity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 地心引力检测表 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2020-05-27
 */
public interface ShareStockGravityMapper extends BaseMapper<ShareStockGravity> {

}
