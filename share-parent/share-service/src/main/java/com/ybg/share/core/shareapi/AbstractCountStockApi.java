package com.ybg.share.core.shareapi;

import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.shareapi.bo.ShareStockCountBO;
import io.netty.util.concurrent.FastThreadLocal;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.List;

/**
 * 统计信息
 */
@Slf4j
public abstract class AbstractCountStockApi {
    FastThreadLocal<ShareStock> stockThreadLocal = new FastThreadLocal<ShareStock>();
    public final FastThreadLocal<ShareStock> getStockThreadLocal() {
        return stockThreadLocal;
    }
    FastThreadLocal<String> timeStrParam = new FastThreadLocal<String>();
    public final FastThreadLocal<String> getTimeThreadLocal() {
        return timeStrParam;
    }

    public Boolean execute(ShareStock stock, String timeStr) {
        stockThreadLocal.set(stock);
        timeStrParam.set(timeStr);
        List<ShareStockDayK> shareStockDayKS = getdayKList(timeStr);
       // log.info("数据量："+shareStockDayKS.size());
        ShareStockCountBO bo = getBO(stock, shareStockDayKS);

        if(bo!=null){
          // log.info("数据量：bo="+bo.toString());
            saveToDB(bo);
           // ThreadUtil.execute(()->{ saveToDB(bo); });

        }

        return Boolean.TRUE;
    }

    public abstract List<ShareStockDayK> getdayKList(String timeStr);


    public ShareStockCountBO getBO(ShareStock stock, List<ShareStockDayK> dayKList) {

        if (dayKList == null || dayKList.size() == 0) {
          //  log.info("没有数据");
            return null;
        }
        Integer upTimes = 0;
        Integer dounTimes = 0;
        ShareStockDayK maxDayK = null;
        ShareStockDayK minDayK = null;
        for (ShareStockDayK shareStockDayK : dayKList) {
            if (maxDayK == null) {
                maxDayK = shareStockDayK;
            }
            if (minDayK == null) {
                minDayK = shareStockDayK;
            }
            if (shareStockDayK.getChangeRange().compareTo(BigDecimal.ZERO) > 0) {
                upTimes++;
            }
            if (shareStockDayK.getChangeRange().compareTo(BigDecimal.ZERO) < 0) {
                dounTimes++;
            }
            if(maxDayK.getClosePrice().compareTo(shareStockDayK.getClosePrice())<0){
                maxDayK = shareStockDayK;
            }
            if(minDayK.getClosePrice().compareTo(shareStockDayK.getClosePrice())>0){
                minDayK = shareStockDayK;
            }

        }

        ShareStockCountBO bo = new ShareStockCountBO();
        bo.setStockId(stock.getId());
        bo.setCode(stock.getCode());

        bo.setMarket(stock.getMarket());
        bo.setUpTimes(upTimes);
        bo.setDownTimes(dounTimes);
        bo.setTopPrice(maxDayK.getClosePrice());
        bo.setLowPrice(minDayK.getClosePrice());
        bo.setTopDate(maxDayK.getRefDate());
        bo.setLowDate(minDayK.getRefDate());
        return bo;


    }

    public abstract void saveToDB(ShareStockCountBO bo);

}
