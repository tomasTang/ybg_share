package com.ybg.share.schedule;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.framework.dto.AbstractJob;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.utils.ThreadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 修复数据任务(不用经常调用)
 */
@Component
public class FixDataJob {
    @Autowired
    ShareStockService shareStockService;

    /**
     * 更新股票状态和中文拼音
     */
    public void fixStockState() {

        QueryWrapper<ShareStock> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareStock.STATUS, 1);
        // wrapper.isNull(ShareStock.SPELL);
     //   wrapper.apply("length(spell)=0");
        List<ShareStock> list = shareStockService.list(wrapper);
        for (ShareStock shareStock : list) {
//            ThreadUtil.execute(() -> {
//                try {
//                    String result = HttpUtil.post("http://www.zx017.net/api/QueryStock?nm=" + shareStock.getCode(), "");
//                    JSONArray array = JSONObject.parseObject(result).getJSONArray("data");
//                    if (array == null || array.size() == 0) {
//                        ShareStock update = new ShareStock();
//                        update.setId(shareStock.getId());
//                        update.setStatus(false);
//                        shareStockService.updateById(update);
//                    } else {
//                        ShareStock update = new ShareStock();
//                        update.setId(shareStock.getId());
//                        update.setStatus(true);
//                        update.setName(JSONObject.parseObject(result).getJSONArray("data").getJSONObject(0).getString("StockName"));
//                        String stockSpell = JSONObject.parseObject(result).getJSONArray("data").getJSONObject(0).getString("StockSpell");
//                        if (StrUtil.isNotBlank(stockSpell)) {
//                            update.setSpell(stockSpell);
//                        }
//                        shareStockService.updateById(update);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            });
            ThreadUtil.execute(new AbstractJob(null) {
                @Override
                public void execute() {
                    try {
                        String result = HttpUtil.post("http://www.zx017.net/api/QueryStock?nm=" + shareStock.getCode(), "");
                        JSONArray array = JSONObject.parseObject(result).getJSONArray("data");
                        if (array == null || array.size() == 0) {
                            ShareStock update = new ShareStock();
                            update.setId(shareStock.getId());
                            update.setStatus(false);
                            shareStockService.updateById(update);
                        } else {
                            ShareStock update = new ShareStock();
                            update.setId(shareStock.getId());
                            update.setStatus(true);
                            update.setName(JSONObject.parseObject(result).getJSONArray("data").getJSONObject(0).getString("StockName"));
                            String stockSpell = JSONObject.parseObject(result).getJSONArray("data").getJSONObject(0).getString("StockSpell");
                            if (StrUtil.isNotBlank(stockSpell)) {
                                update.setSpell(stockSpell);
                            }
                            shareStockService.updateById(update);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
