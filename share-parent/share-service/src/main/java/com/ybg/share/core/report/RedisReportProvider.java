package com.ybg.share.core.report;


import com.alibaba.fastjson.JSONObject;
import com.bstek.ureport.provider.report.ReportFile;
import com.bstek.ureport.provider.report.ReportProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 基于redis实现
 * @author  deament
 */
public class RedisReportProvider implements ReportProvider, ApplicationContextAware {
    private String prefix = "redis:";
    private String redisPrefix="file:";
    private String fileStoreDir;
    private boolean disabled;
    Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String REDIS_KEY = "reportfile";

    private transient RedisTemplate redisTemplate;

    @Override
    public InputStream loadReport(String file) {
        if(file.startsWith(prefix)){
            file=file.substring(prefix.length(),file.length());
        }
        Object o = redisTemplate.opsForValue().get( redisPrefix+ file);
        if (o != null) {

            return new ByteArrayInputStream(o.toString().getBytes());
        }
        return null;
    }

    @Override
    public void deleteReport(String file) {
        if(file.startsWith(prefix)){
            file=file.substring(prefix.length(),file.length());
        }
        redisTemplate.opsForHash().delete(REDIS_KEY, file);
        redisTemplate.opsForValue().getOperations().delete( redisPrefix+file);
    }

    @Override
    public List<ReportFile> getReportFiles() {
        List<ReportFile> files = new ArrayList<>();
        if (redisTemplate == null) {
            logger.info("error getRedisTemplate is null");
        }
        Map entries = redisTemplate.opsForHash().entries(REDIS_KEY);

        if (entries == null) {
            return files;
        }
        try {
            for (Object o : entries.keySet()) {
                String value = entries.get(o).toString();

                JSONObject jsonObject = JSONObject.parseObject(value);
                ReportFile reportFile =    new ReportFile(jsonObject.getString("name"),jsonObject.getDate("updateDate"));
                files.add(reportFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return files;
    }

    @Override
    public void saveReport(String file, String content) {
        if(file.startsWith(prefix)){
            file=file.substring(prefix.length(),file.length());
        }
        ReportFile reportFile = new ReportFile(file, new Date());
        redisTemplate.opsForHash().put(REDIS_KEY, file, JSONObject.toJSONString(reportFile));
        redisTemplate.opsForValue().set(  redisPrefix+file, content);
    }

    @Override
    public String getName() {
        return "redis存储器";
    }

    @Override
    public boolean disabled() {
        return false;
    }

    @Override
    public String getPrefix() {
        return prefix;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    }

    public RedisReportProvider(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFileStoreDir() {
        return fileStoreDir;
    }

    public void setFileStoreDir(String fileStoreDir) {
        this.fileStoreDir = fileStoreDir;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
}
