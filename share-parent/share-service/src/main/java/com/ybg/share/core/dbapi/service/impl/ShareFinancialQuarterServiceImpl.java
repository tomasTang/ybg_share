package com.ybg.share.core.dbapi.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybg.share.core.dbapi.service.ShareFinancialQuarterService;
import com.ybg.share.core.remoteapi.dto.FinancialDTO;
import com.ybg.share.core.remoteapi.vo.FinancialVO;
import com.ybg.share.core.dbapi.entity.ShareFinancialQuarter;
import com.ybg.share.core.dbapi.dao.ShareFinancialQuarterMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;

/**
 * <p>
 * 财务报表-季度 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-27
 */
//@Service(version = "1.0.0", interfaceClass = ShareFinancialQuarterService.class)
@Service
public class ShareFinancialQuarterServiceImpl extends ServiceImpl<ShareFinancialQuarterMapper, ShareFinancialQuarter> implements ShareFinancialQuarterService {

    @Override
    public Page<FinancialVO> getFinancialOfQuarterByPage(FinancialDTO dto) {
        return null;
    }

    @Override
    public boolean saveIgnore(ShareFinancialQuarter bean) {
        return baseMapper.saveIgnore(bean);
    }
}
