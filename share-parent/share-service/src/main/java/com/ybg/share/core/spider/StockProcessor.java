package com.ybg.share.core.spider;

import com.ybg.framework.dto.AbstractJob;
import com.ybg.framework.enums.MarketEnum;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.utils.ThreadUtil;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import java.util.List;

/**
 * 拉取股票名称数据
 *
 * @author Maokun.zhong
 */
@Slf4j
public class StockProcessor implements PageProcessor {


    @Setter
    private ShareStockService shareStockService;
    public static final String STOCK_LIST = "http://quote.eastmoney.com/stock_list.html";
    /// private static int count = 0;
    /**
     * 抓取网站的相关配置，包括编码、抓取间隔、重试次数等
     */
    private Site site = Site
            .me()
            .setDomain("web")
            .setSleepTime(3000)//处理页面之间的时间间隔，单位是微秒
            .setUserAgent(
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) " +
                            "AppleWebKit/537.31 (KHTML, like Gecko) " +
                            "Chrome/26.0.1410.65 Safari/537.31")
            .setTimeOut(10000)//超时时间
            .setRetryTimes(3)//重试次数
            .setRetrySleepTime(3000)//重试间隔
            ;

    private void insert(List<Selectable> nodes, String code) {
        for (Selectable node : nodes) {
            Selectable selectable = node.nodes().get(0);
            String text = selectable.xpath("a/text()").get();
            String[] nameAndCode = text.replace("(", ",").replace(")", "").split(",");
            ShareStock shareStock = new ShareStock();
            shareStock.setName(nameAndCode[0]);
            shareStock.setCode(nameAndCode[1]);
            shareStock.setMarket(code);
            // System.out.println(nameAndCode[0]+","+nameAndCode[1]);
            //调用jdbc方法，存入数据库。
            if (null == shareStockService) {
                log.info("业务层注入失败");
                return;
            }

            ThreadUtil.execute(new AbstractJob(shareStock) {
                @Override
                public void execute() {
                    ShareStock shareStock = (ShareStock) getParams();
                    shareStockService.saveIgnore(shareStock);
                }
            });
//            if (!shareStockService.saveIgnore(shareStock)) {
//              //  count++;
//            }
        }
    }

    @Override
    public void process(Page page) {



        //列表页
        if (page.getUrl().regex(STOCK_LIST).match()) {
            //深圳
            List<Selectable> nodes = page.getHtml().xpath("/html/body/div[9]/div[2]/div[1]/ul[2]/").nodes();
            insert(nodes, MarketEnum.sz.getCode());
            //上海
            nodes = page.getHtml().xpath("/html/body/div[9]/div[2]/div[1]/ul[1]/").nodes();
            insert(nodes, MarketEnum.sh.getCode());
            //  System.out.println("插入失败：" + count + " 条数据。");
        }
    }

    @Override
    public Site getSite() {
        return site;
    }


}
