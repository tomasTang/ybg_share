package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.entity.ShareStockEmailDataHistory;
import com.ybg.share.core.dbapi.dao.ShareStockEmailDataHistoryMapper;
import com.ybg.share.core.dbapi.service.ShareStockEmailDataHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2020-01-06
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockEmailDataHistoryService.class)
@Service
public class ShareStockEmailDataHistoryServiceImpl extends ServiceImpl<ShareStockEmailDataHistoryMapper, ShareStockEmailDataHistory> implements ShareStockEmailDataHistoryService {

}
