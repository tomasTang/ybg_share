package com.ybg.share.core.remoteapi;

import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.remoteapi.vo.StockReportVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class StockReportRemoteApiImpl implements  StockReportRemoteApi {
    @Autowired
    private ShareStockService shareStockService;

    @Override
    public StockReportVO getStockReport(Long stockId) {
        ShareStock shareStock = shareStockService.getById(stockId);
        StockReportVO vo =  new StockReportVO();
        vo.setStockId(shareStock.getId());
        vo.setStockName(shareStock.getName());
        vo.setStockCode(shareStock.getCode());
        vo.setEvaluate("");
        vo.setGravity("");
        vo.setFinancial("");
        vo.setStockIndexInfo("");

        return vo;
    }
}
