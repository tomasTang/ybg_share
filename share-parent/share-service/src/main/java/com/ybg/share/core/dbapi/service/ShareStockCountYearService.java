package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareStockCountYear;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 个股年度统计 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-31
 */
public interface ShareStockCountYearService extends IService<ShareStockCountYear> {

    int saveReplace(ShareStockCountYear entity);
}
