package com.ybg.share.core.dbapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.remoteapi.dto.StockPageQueryDTO;
import com.ybg.share.core.remoteapi.vo.ShareStockVO;
import com.ybg.share.core.dbapi.dao.ShareStockMapper;
import com.ybg.share.core.dbapi.entity.ShareStock;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 股票信息 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
@Service
public class ShareStockServiceImpl extends ServiceImpl<ShareStockMapper, ShareStock> implements ShareStockService {


    @Override
    public boolean saveIgnore(ShareStock shareStock) {
        return baseMapper.saveIgnore(shareStock);
    }

    @Override
    public boolean saveReplace(ShareStock shareStock) {
        return baseMapper.saveReplace(shareStock);
    }

    @Override
    public ShareStock getByCode(String stock) {
        QueryWrapper<ShareStock> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareStock.CODE, stock);
        return this.getOne(wrapper);
    }

    @Override
    public void initSystem(String sql) {
        baseMapper.initSystem(sql);
    }

    @Override
    public Page<ShareStockVO> pageStock(StockPageQueryDTO dto) {
        Page<ShareStockVO> page= new Page<>(dto.getCurrentPage(),dto.getSize());
        page.setRecords(baseMapper.listStock(dto));
        page.setTotal(baseMapper.countStock(dto));
        return page;
    }


}
