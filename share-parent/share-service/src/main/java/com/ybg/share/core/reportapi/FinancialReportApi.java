package com.ybg.share.core.reportapi;

import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvUtil;
import com.ybg.share.core.dbapi.entity.ShareFinancialQuarter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 财务报表api
 */
@Component("financialReportApi")
@Slf4j
public class FinancialReportApi {


    /**
     * 获取季度 报告
     * @param dataSource
     * @param table
     * @param params
     */
    public  List<ShareFinancialQuarter> getQuarterInfo(String dataSource, String table, Map<String,String> params) {
        List<ShareFinancialQuarter> data= new ArrayList<>();
        String stockCode = params.get("code");
        String url = "http://quotes.money.163.com/service/zycwzb_" + stockCode + ".html?type=season";
        CloseableHttpClient client = null;
        CloseableHttpResponse response;
        try {
            HttpGet httpGet = new HttpGet(url);
            client = HttpClients.createDefault();
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            BufferedReader in = new BufferedReader(new InputStreamReader(entity.getContent(), "GBK"));
            CsvData csvData = CsvUtil.getReader().read(in);
            if (csvData == null) {
                log.info("数据为空");
                return data;
            }
            int headerSize = csvData.getRow(0).getRawList().size();
            //实际拥有的报告数
            int dataSize = headerSize - 1;
            if (dataSize == 0) {
                return data;
            }
            for (int rowi = 1; rowi < dataSize; rowi++) {
                int count = 0;
                ShareFinancialQuarter bean = new ShareFinancialQuarter();
                bean.setReportDate(csvData.getRow(count++).get(rowi));
//                bean.setStockId(stock.getId());
//                bean.setCode(stock.getCode());
                bean.setBasicEps(toNum(csvData.getRow(count++).get(rowi)));
                bean.setEps(toNum(csvData.getRow(count++).get(rowi)));
                bean.setCashFlow((csvData.getRow(count++).get(rowi)));
                bean.setRevenue(toNum(csvData.getRow(count++).get(rowi)));
                bean.setProfitability(toNum(csvData.getRow(count++).get(rowi)));
                bean.setOperatingProfit(toNum(csvData.getRow(count++).get(rowi)));
                bean.setInvestmentReturn(toNum(csvData.getRow(count++).get(rowi)));
                bean.setNonOperatingIncome(toNum(csvData.getRow(count++).get(rowi)));
                bean.setTotalProfit(toNum(csvData.getRow(count++).get(rowi)));
                bean.setNetProfit(toNum(csvData.getRow(count++).get(rowi)));
                bean.setDeductProfit(toNum(csvData.getRow(count++).get(rowi)));
                bean.setNetCashFlow(toNum(csvData.getRow(count++).get(rowi)));
                bean.setNetIncreaseCash(toNum(csvData.getRow(count++).get(rowi)));
                bean.setTotalAssets(toNum(csvData.getRow(count++).get(rowi)));
                bean.setCurrentAssets(toNum(csvData.getRow(count++).get(rowi)));
                bean.setTotalLiabilities(toNum(csvData.getRow(count++).get(rowi)));
                bean.setCurrentLiabilities(toNum(csvData.getRow(count++).get(rowi)));
                bean.setStockholderEquity(toNum(csvData.getRow(count++).get(rowi)));
                bean.setWeightedReturnEquity(toNum(csvData.getRow(count++).get(rowi)));
//                bean.setMarket(stock.getMarket());
                data.add(bean);

            }
            if (response != null) {
                response.close();
            }
            if (client != null) {
                client.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return data;

    }


    /**
     * 获取年度 报告
     * @param dataSource
     * @param table
     * @param params
     */
    public  List<ShareFinancialQuarter> getYearInfo(String dataSource, String table, Map<String,String> params) {
        List<ShareFinancialQuarter> data= new ArrayList<>();
        String stockCode = params.get("code");
        String url = "http://quotes.money.163.com/service/zycwzb_" + stockCode + ".html?type=season";
        CloseableHttpClient client = null;
        CloseableHttpResponse response;
        try {
            HttpGet httpGet = new HttpGet(url);
            client = HttpClients.createDefault();
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            BufferedReader in = new BufferedReader(new InputStreamReader(entity.getContent(), "GBK"));
            CsvData csvData = CsvUtil.getReader().read(in);
            if (csvData == null) {
                log.info("数据为空");
                return data;
            }
            int headerSize = csvData.getRow(0).getRawList().size();
            //实际拥有的报告数
            int dataSize = headerSize - 1;
            if (dataSize == 0) {
                return data;
            }


            for (int rowi = 1; rowi < dataSize; rowi++) {
                int count = 0;
                ShareFinancialQuarter bean = new ShareFinancialQuarter();
                bean.setReportDate(csvData.getRow(count++).get(rowi));
//                bean.setStockId(stock.getId());
//                bean.setCode(stock.getCode());
                bean.setBasicEps(toNum(csvData.getRow(count++).get(rowi)));
                bean.setEps(toNum(csvData.getRow(count++).get(rowi)));
                bean.setCashFlow((csvData.getRow(count++).get(rowi)));
                bean.setRevenue(toNum(csvData.getRow(count++).get(rowi)));
                bean.setProfitability(toNum(csvData.getRow(count++).get(rowi)));
                bean.setOperatingProfit(toNum(csvData.getRow(count++).get(rowi)));
                bean.setInvestmentReturn(toNum(csvData.getRow(count++).get(rowi)));
                bean.setNonOperatingIncome(toNum(csvData.getRow(count++).get(rowi)));
                bean.setTotalProfit(toNum(csvData.getRow(count++).get(rowi)));
                bean.setNetProfit(toNum(csvData.getRow(count++).get(rowi)));
                bean.setDeductProfit(toNum(csvData.getRow(count++).get(rowi)));
                bean.setNetCashFlow(toNum(csvData.getRow(count++).get(rowi)));
                bean.setNetIncreaseCash(toNum(csvData.getRow(count++).get(rowi)));
                bean.setTotalAssets(toNum(csvData.getRow(count++).get(rowi)));
                bean.setCurrentAssets(toNum(csvData.getRow(count++).get(rowi)));
                bean.setTotalLiabilities(toNum(csvData.getRow(count++).get(rowi)));
                bean.setCurrentLiabilities(toNum(csvData.getRow(count++).get(rowi)));
                bean.setStockholderEquity(toNum(csvData.getRow(count++).get(rowi)));
                bean.setWeightedReturnEquity(toNum(csvData.getRow(count++).get(rowi)));
//                bean.setMarket(stock.getMarket());
                data.add(bean);

            }
            if (response != null) {
                response.close();
            }
            if (client != null) {
                client.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return data;

    }




    private BigDecimal toNum(String value) {
        try {
            return new BigDecimal(value);
        } catch (Exception e) {

        }
        return BigDecimal.ZERO;

    }
}
