package com.ybg.share.core.remoteapi.datamapper;

import com.ybg.share.core.dbapi.entity.ShareFinancialQuarter;
import com.ybg.share.core.dbapi.entity.ShareFinancialYear;
import com.ybg.share.core.remoteapi.vo.FinancialVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface StockFinancialDataMapper {
    StockFinancialDataMapper INSTANCE= Mappers.getMapper(StockFinancialDataMapper.class);

    FinancialVO toYearVO(ShareFinancialYear bean);
    FinancialVO toMonthVO(ShareFinancialQuarter bean);

    List<FinancialVO> toYearVOList(List<ShareFinancialYear> list);
    List<FinancialVO> toMonthVOList(List<ShareFinancialQuarter> list);
}
