package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareFixDaykTask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 日K自我修复任务 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-12-30
 */
public interface ShareFixDaykTaskService extends IService<ShareFixDaykTask> {
    /**
     * 初始化任务列表
     */
    void initList();

    int getMinTime();
}
