package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareStockGravity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 地心引力检测表 服务类
 * </p>
 *
 * @author yanyu
 * @since 2020-05-27
 */
public interface ShareStockGravityService extends IService<ShareStockGravity> {

}
