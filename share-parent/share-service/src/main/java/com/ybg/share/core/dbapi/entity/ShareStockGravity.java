package com.ybg.share.core.dbapi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * <p>
 * 地心引力检测表
 * </p>
 *
 * @author yanyu
 * @since 2020-10-27
 */
@ApiModel(value = "地心引力检测表")
public class ShareStockGravity extends Model<ShareStockGravity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 股票ID
     */
    @ApiModelProperty(value = "股票ID")
    private Long stockId;
    /**
     * 股票名称
     */
    @ApiModelProperty(value = "股票名称")
    private String stockName;
    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String stockCode;
    /**
     * 当前价
     */
    @ApiModelProperty(value = "当前价")
    private BigDecimal nowPrice;
    /**
     * 引力线价格
     */
    @ApiModelProperty(value = "引力线价格")
    private BigDecimal linePrice;
    /**
     * 引力线价格*1.1
     */
    @ApiModelProperty(value = "引力线价格*1.1")
    private BigDecimal linePrice110;
    /**
     * 引力线价格*1.2
     */
    @ApiModelProperty(value = "引力线价格*1.2")
    private BigDecimal linePrice120;
    /**
     * 引力线价格*0.9
     */
    @ApiModelProperty(value = "引力线价格*0.9")
    private BigDecimal linePrice90;
    /**
     * 引力线价格*0.8
     */
    @ApiModelProperty(value = "引力线价格*0.8")
    private BigDecimal linePrice80;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /**
     * 当前价 和地心引力线比率
     */
    @ApiModelProperty(value = "当前价 和地心引力线比率")
    private BigDecimal rate;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取股票ID
     */
    public Long getStockId() {
        return stockId;
    }

    /**
     * 设置股票ID
     */

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    /**
     * 获取股票名称
     */
    public String getStockName() {
        return stockName;
    }

    /**
     * 设置股票名称
     */

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    /**
     * 获取股票代码
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * 设置股票代码
     */

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    /**
     * 获取当前价
     */
    public BigDecimal getNowPrice() {
        return nowPrice;
    }

    /**
     * 设置当前价
     */

    public void setNowPrice(BigDecimal nowPrice) {
        this.nowPrice = nowPrice;
    }

    /**
     * 获取引力线价格
     */
    public BigDecimal getLinePrice() {
        return linePrice;
    }

    /**
     * 设置引力线价格
     */

    public void setLinePrice(BigDecimal linePrice) {
        this.linePrice = linePrice;
    }

    /**
     * 获取引力线价格*1.1
     */
    public BigDecimal getLinePrice110() {
        return linePrice110;
    }

    /**
     * 设置引力线价格*1.1
     */

    public void setLinePrice110(BigDecimal linePrice110) {
        this.linePrice110 = linePrice110;
    }

    /**
     * 获取引力线价格*1.2
     */
    public BigDecimal getLinePrice120() {
        return linePrice120;
    }

    /**
     * 设置引力线价格*1.2
     */

    public void setLinePrice120(BigDecimal linePrice120) {
        this.linePrice120 = linePrice120;
    }

    /**
     * 获取引力线价格*0.9
     */
    public BigDecimal getLinePrice90() {
        return linePrice90;
    }

    /**
     * 设置引力线价格*0.9
     */

    public void setLinePrice90(BigDecimal linePrice90) {
        this.linePrice90 = linePrice90;
    }

    /**
     * 获取引力线价格*0.8
     */
    public BigDecimal getLinePrice80() {
        return linePrice80;
    }

    /**
     * 设置引力线价格*0.8
     */

    public void setLinePrice80(BigDecimal linePrice80) {
        this.linePrice80 = linePrice80;
    }

    /**
     * 获取描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     */

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     */

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取当前价 和地心引力线比率
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * 设置当前价 和地心引力线比率
     */

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 股票ID列的数据库字段名称
     */
    public static final String STOCK_ID = "stock_id";

    /**
     * 股票名称列的数据库字段名称
     */
    public static final String STOCK_NAME = "stock_name";

    /**
     * 股票代码列的数据库字段名称
     */
    public static final String STOCK_CODE = "stock_code";

    /**
     * 当前价列的数据库字段名称
     */
    public static final String NOW_PRICE = "now_price";

    /**
     * 引力线价格列的数据库字段名称
     */
    public static final String LINE_PRICE = "line_price";

    /**
     * 引力线价格*1.1列的数据库字段名称
     */
    public static final String LINE_PRICE110 = "line_price110";

    /**
     * 引力线价格*1.2列的数据库字段名称
     */
    public static final String LINE_PRICE120 = "line_price120";

    /**
     * 引力线价格*0.9列的数据库字段名称
     */
    public static final String LINE_PRICE90 = "line_price90";

    /**
     * 引力线价格*0.8列的数据库字段名称
     */
    public static final String LINE_PRICE80 = "line_price80";

    /**
     * 描述列的数据库字段名称
     */
    public static final String DESCRIPTION = "description";

    /**
     * 更新时间列的数据库字段名称
     */
    public static final String UPDATE_TIME = "update_time";

    /**
     * 当前价 和地心引力线比率列的数据库字段名称
     */
    public static final String RATE = "rate";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareStockGravity{" +
                "id=" + id +
                ", stockId=" + stockId +
                ", stockName=" + stockName +
                ", stockCode=" + stockCode +
                ", nowPrice=" + nowPrice +
                ", linePrice=" + linePrice +
                ", linePrice110=" + linePrice110 +
                ", linePrice120=" + linePrice120 +
                ", linePrice90=" + linePrice90 +
                ", linePrice80=" + linePrice80 +
                ", description=" + description +
                ", updateTime=" + updateTime +
                ", rate=" + rate +
                "}";
    }
}
