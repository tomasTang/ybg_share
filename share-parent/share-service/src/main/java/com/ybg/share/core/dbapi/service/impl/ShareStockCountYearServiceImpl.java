package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.service.ShareStockCountYearService;
import com.ybg.share.core.dbapi.entity.ShareStockCountYear;
import com.ybg.share.core.dbapi.dao.ShareStockCountYearMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 个股年度统计 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-31
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockCountYearService.class)
@Service
public class ShareStockCountYearServiceImpl extends ServiceImpl<ShareStockCountYearMapper, ShareStockCountYear> implements ShareStockCountYearService {

    @Override
    public int saveReplace(ShareStockCountYear entity) {
        return baseMapper.saveReplace(entity);
    }
}
