package com.ybg.share.core.dbapi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.remoteapi.vo.StockIndexDayKVO;
import com.ybg.share.core.dbapi.dao.sql.ShareStockIndexSQL;
import com.ybg.share.core.remoteapi.dto.StockIndexDTO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * <p>
 * 上证指数和深证指数 Mapper 接口
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-17
 */
public interface ShareStockIndexMapper extends BaseMapper<ShareStockIndex> {

    @Insert("insert ignore into share_stock_index " +
            "(`ref_date`,`code`,`market`,`stock_name`,`close_price`,`max_price`,`min_price`,`open_price`," +
            "`before_close`,`change_amount`,`change_range`,`trade_num`,`trade_money`) values" +
            "(#{refDate},#{code},#{market},#{stockName},#{closePrice},#{maxPrice},#{minPrice},#{openPrice}," +
            "#{beforeClose},#{changeAmount},#{changeRange},#{tradeNum},#{tradeMoney})")
    boolean insertIgnore(ShareStockIndex shareStockIndex);

    @SelectProvider(type = ShareStockIndexSQL.class,method = "listStockDayK")
    List<StockIndexDayKVO> listStockDayK(@Param("dto") StockIndexDTO dto);

    @SelectProvider(type = ShareStockIndexSQL.class,method = "countStockDayK")
    Long countStockDayK(@Param("dto") StockIndexDTO dto);
}
