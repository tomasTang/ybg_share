package com.ybg.share.core.shareapi;

import com.ybg.share.core.shareapi.bo.ShareStockDayBO;
import com.ybg.share.core.shareapi.dto.QueryDayDTO;

import java.util.List;

/**
 * 市场交易日K数据接口
 */
public abstract class AbstractMarketApi {
    public abstract List<ShareStockDayBO> getDayDate(QueryDayDTO dto);
}
