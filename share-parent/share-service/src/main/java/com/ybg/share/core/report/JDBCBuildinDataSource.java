package com.ybg.share.core.report;

import com.bstek.ureport.definition.datasource.BuildinDatasource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * JDBC
 * @author deament
 */
public class JDBCBuildinDataSource implements BuildinDatasource {
    private DataSource dataSource;
    private String name;
    @Override
    public String name() {
        return name;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JDBCBuildinDataSource(DataSource dataSource, String name) {
        this.dataSource = dataSource;
        this.name = name;
    }

    public JDBCBuildinDataSource() {
    }

    @Override
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
