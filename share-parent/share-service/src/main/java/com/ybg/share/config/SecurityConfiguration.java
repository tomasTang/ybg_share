package com.ybg.share.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author  deament
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Slf4j
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeRequests().antMatchers(HttpMethod.OPTIONS).permitAll().anyRequest().authenticated().and()
//				.httpBasic().and().csrf().disable();
        String skipURL = getApplicationContext().getEnvironment().getProperty("oauth.skipURL");
        String[] spipURLs= skipURL.split(",");
        for (String spipURL : spipURLs) {
            log.info("跳过的鉴权URL:"+spipURL);
        }

        http.authorizeRequests().antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers(HttpMethod.GET,spipURLs).permitAll()
                .antMatchers("/ureport/designer").authenticated()
                .anyRequest().authenticated().and().httpBasic()
                .and().csrf().disable() ;
        //ureport 需要启动这个禁用。否则表单设计器无法正常使用
        http.headers()
                .frameOptions().sameOrigin()
                .httpStrictTransportSecurity().disable();
    }

}
