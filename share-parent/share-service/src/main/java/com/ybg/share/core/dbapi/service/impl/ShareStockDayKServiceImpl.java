package com.ybg.share.core.dbapi.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.share.core.dbapi.dao.ShareStockDayKMapper;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;
import com.ybg.share.core.remoteapi.dto.StockDayKDTO;
import com.ybg.framework.dto.AbstractJob;
import com.ybg.share.config.SystemInit;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.remoteapi.vo.StockDayKVO;
import com.ybg.share.utils.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;

/**
 * <p>
 * 行情日k数据 服务实现类
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-21
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockDayKService.class)
@Service
@Slf4j
public class ShareStockDayKServiceImpl extends ServiceImpl<ShareStockDayKMapper, ShareStockDayK> implements ShareStockDayKService {



    @Override
    public boolean saveIgnore(ShareStockDayK shareStockDayK) {
        return baseMapper.saveIgnore(shareStockDayK);
    }

    @Override
    public ShareStockDayK getMaxDateByCode(String code, Long stockId) {
        return baseMapper.getMaxDateByCode(code, stockId);
    }

    @Override
    public int initTable() {
//        ThreadUtil.execute(()->{
//            for (int i = 0; i < SystemInit.SHARE_STOCK_DAYK_TABLESHARDING; i++) {
//                baseMapper.initTable(i);
//            }
//        });
        ThreadUtil.execute(new AbstractJob(null) {
            @Override
            public void execute() {

                for (int i = 0; i < SystemInit.SHARE_STOCK_DAYK_TABLESHARDING; i++) {
                    baseMapper.initTable(i);
                }
            }
        });
        return 1;
    }

    @Override
    public Page<StockDayKVO> page(StockDayKDTO dto) {
        Page<StockDayKVO> page= new Page<>(dto.getCurrentPage(),dto.getSize());
        try {
            page.setRecords(this.baseMapper.listStockDayK(dto));
            page.setTotal(this.baseMapper.countStockDayK(dto));
        }catch (Exception e){
            log.error("{}",e);
            e.printStackTrace();
        }
        return page;
    }

    @Override
    public ShareStockDayK getLastDayKByCode(String code, Long id) {
        return baseMapper.getLastDayKByCode(code,id);
    }
}
