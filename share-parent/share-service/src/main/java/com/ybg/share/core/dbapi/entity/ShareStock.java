package com.ybg.share.core.dbapi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * <p>
 * 股票信息
 * </p>
 *
 * @author yanyu
 * @since 2020-01-27
 */
@ApiModel(value = "股票信息")
public class ShareStock extends Model<ShareStock> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 股票名称
     */
    @ApiModelProperty(value = "股票名称")
    private String name;
    /**
     * 1 可用，0禁用
     */
    @ApiModelProperty(value = "1 可用，0禁用")
    private String code;
    /**
     * 所属市场
     */
    @ApiModelProperty(value = "所属市场")
    private String market;
    /**
     * 1 可用 0不可用
     */
    @ApiModelProperty(value = "1 可用 0不可用")
    private Boolean status;
    /**
     * 股票拼音
     */
    @ApiModelProperty(value = "股票拼音")
    private String spell;
    /**
     * 所属行业
     */
    @ApiModelProperty(value = "所属行业")
    private String cindustry;
    /**
     * 企业名字
     */
    @ApiModelProperty(value = "企业名字")
    private String companyName;
    /**
     * 注册资本（万元）
     */
    @ApiModelProperty(value = "注册资本（万元）")
    private BigDecimal registeredCapital;
    /**
     * 公司电话
     */
    @ApiModelProperty(value = "公司电话")
    private String tel;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取股票名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置股票名称
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取1 可用，0禁用
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置1 可用，0禁用
     */

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取所属市场
     */
    public String getMarket() {
        return market;
    }

    /**
     * 设置所属市场
     */

    public void setMarket(String market) {
        this.market = market;
    }

    /**
     * 获取1 可用 0不可用
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置1 可用 0不可用
     */

    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 获取股票拼音
     */
    public String getSpell() {
        return spell;
    }

    /**
     * 设置股票拼音
     */

    public void setSpell(String spell) {
        this.spell = spell;
    }

    /**
     * 获取所属行业
     */
    public String getCindustry() {
        return cindustry;
    }

    /**
     * 设置所属行业
     */

    public void setCindustry(String cindustry) {
        this.cindustry = cindustry;
    }

    /**
     * 获取企业名字
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * 设置企业名字
     */

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * 获取注册资本（万元）
     */
    public BigDecimal getRegisteredCapital() {
        return registeredCapital;
    }

    /**
     * 设置注册资本（万元）
     */

    public void setRegisteredCapital(BigDecimal registeredCapital) {
        this.registeredCapital = registeredCapital;
    }

    /**
     * 获取公司电话
     */
    public String getTel() {
        return tel;
    }

    /**
     * 设置公司电话
     */

    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 股票名称列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * 1 可用，0禁用列的数据库字段名称
     */
    public static final String CODE = "code";

    /**
     * 所属市场列的数据库字段名称
     */
    public static final String MARKET = "market";

    /**
     * 1 可用 0不可用列的数据库字段名称
     */
    public static final String STATUS = "status";

    /**
     * 股票拼音列的数据库字段名称
     */
    public static final String SPELL = "spell";

    /**
     * 所属行业列的数据库字段名称
     */
    public static final String CINDUSTRY = "cindustry";

    /**
     * 企业名字列的数据库字段名称
     */
    public static final String COMPANY_NAME = "company_name";

    /**
     * 注册资本（万元）列的数据库字段名称
     */
    public static final String REGISTERED_CAPITAL = "registered_capital";

    /**
     * 公司电话列的数据库字段名称
     */
    public static final String TEL = "tel";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareStock{" +
                "id=" + id +
                ", name=" + name +
                ", code=" + code +
                ", market=" + market +
                ", status=" + status +
                ", spell=" + spell +
                ", cindustry=" + cindustry +
                ", companyName=" + companyName +
                ", registeredCapital=" + registeredCapital +
                ", tel=" + tel +
                "}";
    }
}
