package com.ybg.share.core.dbapi.dao.sql;

import com.ybg.share.core.dbapi.entity.ShareStockCountYear;
import org.apache.ibatis.annotations.Param;

public class ShareStockCountYearSQL {
    public String saveReplace(@Param("e") ShareStockCountYear entity) {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO share_stock_count_year(stock_id,ref_date,code,market,up_times,down_times,top_price,low_price,top_date,low_date,create_time)");
        sql.append(" VALUES(#{e.stockId},#{e.refDate},#{e.code},#{e.market},#{e.upTimes},#{e.downTimes},#{e.topPrice},#{e.lowPrice},#{e.topDate},#{e.lowDate},#{e.createTime})");
        sql.append(" ON DUPLICATE KEY UPDATE stock_id=VALUES( stock_id),ref_date=VALUES( ref_date) , code=VALUES( code),market=VALUES( market),up_times=VALUES( up_times),down_times=VALUES( down_times),top_price=VALUES( top_price),low_price=VALUES( low_price),top_date=VALUES( top_date),low_date=VALUES( low_date),create_time=VALUES( create_time)");

        return sql.toString();
    }
}
