package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareStockCountMonth;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 个股月度统计 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-31
 */
public interface ShareStockCountMonthService extends IService<ShareStockCountMonth> {

    /**
     * 系统初始化 初始化分表
     */
     void initTable();

    int saveReplace(ShareStockCountMonth entity);
}
