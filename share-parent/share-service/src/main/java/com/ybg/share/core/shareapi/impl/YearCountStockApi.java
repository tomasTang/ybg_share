package com.ybg.share.core.shareapi.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.share.core.shareapi.AbstractCountStockApi;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.entity.ShareStockCountYear;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.dbapi.service.ShareStockCountYearService;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;
import com.ybg.share.core.shareapi.bo.ShareStockCountBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 年度统计
 */
@Service
public class YearCountStockApi extends AbstractCountStockApi {

    @Autowired
    ShareStockDayKService shareStockDayKService;
    @Autowired
    ShareStockCountYearService shareStockCountYearService;

    @Override
    public List<ShareStockDayK> getdayKList(String timeStr) {
        ShareStock shareStock= getStockThreadLocal().get();
        QueryWrapper<ShareStockDayK> wrapper= new QueryWrapper<>();
        wrapper.eq(ShareStockDayK.STOCK_ID,shareStock.getId());
        wrapper.ge(ShareStockDayK.REF_DATE,timeStr+"-01-01");
        wrapper.le(ShareStockDayK.REF_DATE,timeStr+"-12-31");
        return  shareStockDayKService.list(wrapper);
    }

    @Override
    public void saveToDB(ShareStockCountBO bo) {
        ShareStockCountYear entity= new ShareStockCountYear();
        entity.setStockId(bo.getStockId());
        entity.setRefDate(getTimeThreadLocal().get());
        entity.setCode(bo.getCode());
        entity.setMarket(bo.getMarket());
        entity.setUpTimes(bo.getUpTimes());
        entity.setDownTimes(bo.getDownTimes());
        entity.setTopPrice(bo.getTopPrice());
        entity.setLowPrice(bo.getLowPrice());
        entity.setTopDate(bo.getTopDate());
        entity.setLowDate(bo.getLowDate());
        entity.setCreateTime(LocalDateTime.now());
        shareStockCountYearService.saveReplace(entity);
    }
}
