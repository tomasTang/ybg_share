package com.ybg.share.core.dbapi.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.share.core.dbapi.service.ShareStockIndexService;
import com.ybg.share.core.remoteapi.vo.StockIndexDayKVO;
import com.ybg.share.core.dbapi.dao.ShareStockIndexMapper;
import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.remoteapi.dto.StockIndexDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;

/**
 * <p>
 * 上证指数和深证指数 服务实现类
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-17
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockIndexService.class)
@Service
@Slf4j
public class ShareStockIndexServiceImpl extends ServiceImpl<ShareStockIndexMapper, ShareStockIndex> implements ShareStockIndexService {

    @Override
    public boolean insertIgnore(ShareStockIndex shareStockIndex) {
        return baseMapper.insertIgnore(shareStockIndex);
    }

    @Override
    public Page<StockIndexDayKVO> pageStock(StockIndexDTO dto) {
        Page<StockIndexDayKVO> page= new Page<>(dto.getCurrentPage(),dto.getSize());
        try {
            page.setRecords(this.baseMapper.listStockDayK(dto));
            page.setTotal(this.baseMapper.countStockDayK(dto));
        }catch (Exception e){
            log.error("{}",e);
            e.printStackTrace();
        }
        return page;
    }
}
