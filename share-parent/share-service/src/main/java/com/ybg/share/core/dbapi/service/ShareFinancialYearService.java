package com.ybg.share.core.dbapi.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybg.share.core.remoteapi.dto.FinancialDTO;
import com.ybg.share.core.remoteapi.vo.FinancialVO;
import com.ybg.share.core.dbapi.entity.ShareFinancialYear;

import java.util.List;

/**
 * <p>
 * 财务报表 年度 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-27
 */
public interface ShareFinancialYearService extends IService<ShareFinancialYear> {

    boolean saveIgnore(ShareFinancialYear bean);

    public Page<FinancialVO> getFinancialOfYearByPage(FinancialDTO dto);

    ShareFinancialYear getByYear(Long stockId,int year);

    List<ShareFinancialYear> getByYear(int year);
}
