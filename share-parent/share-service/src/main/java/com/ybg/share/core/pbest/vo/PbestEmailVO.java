package com.ybg.share.core.pbest.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class PbestEmailVO implements Serializable {
    /**
     * 股票ID
     */
    @ApiModelProperty(value = "股票ID")
    private Long stockId;
    /**
     * 年份
     */
      @ApiModelProperty(value = "年份")
      private Integer refYear;

    /**
     * 股票代码
     */
    @Excel(name = "股票代码", orderNum = "1")
    @ApiModelProperty(value = "股票代码")
    private String stockCode;
    /**
     * 股票名称
     */
    @Excel(name = "股票名称", orderNum = "2")
    @ApiModelProperty(value = "股票名称")
    private String stockName;

     /**
     * 买入价格
     */
    @Excel(name = "买入价格", orderNum = "6")
    @ApiModelProperty(value = "买入价格")
    private BigDecimal buyPrice;
    /**
     * 卖出价格
     */
    @Excel(name = "卖出价格", orderNum = "7")
    @ApiModelProperty(value = "卖出价格")
    private BigDecimal sellPrice;
    /**
     * 算法名称
     */
    @ApiModelProperty(value = "算法名称")
    private String pbestName;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 最大次数
     */
    @Excel(name = "最大次数", orderNum = "3")
    @ApiModelProperty(value = "最大次数")
    private Integer times;
    /**
     * 盈利
     */
    @Excel(name = "盈利", orderNum = "47")
    @ApiModelProperty(value = "盈利")
    private BigDecimal profit;
    /**
     * 利润率
     */
    @Excel(name = "利润率", orderNum = "5")
    @ApiModelProperty(value = "利润率")
    private BigDecimal profitRate;

    /**
     * 市场
     */
    @Excel(name = "市场", orderNum = "8")
    @ApiModelProperty(value = "市场")
    private String market;

    @Excel(name = "当前价", orderNum = "9")
    @ApiModelProperty(value = "当前价，取自前一个日K收盘价")
    private BigDecimal price;
    @Excel(name = "差异比", orderNum = "10")
    @ApiModelProperty(value = "差异比,数字越大则利润率越小")
    private BigDecimal oddsRatio;


    /**
     * 关注指数
     */
    @Excel(name = "关注指数", orderNum = "11")
    @ApiModelProperty(value = "关注指数")
    private BigDecimal focus;
    /**
     * 机构参与度
     */
    @Excel(name = "机构参与度", orderNum = "12")
    @ApiModelProperty(value = "机构参与度")
    private BigDecimal jgcyd;
    /**
     * 季度参与度描述
     */
    @Excel(name = "机构参与度描述", orderNum = "13")
    @ApiModelProperty(value = "机构参与度描述")
    private String jgcydType;
    /**
     * 综合得分
     */
    @Excel(name = "综合得分", orderNum = "14")
    @ApiModelProperty(value = "综合得分")
    private BigDecimal totalScore;
    /**
     * 当前排名
     */
    @Excel(name = "当前排名", orderNum = "15")
    @ApiModelProperty(value = "当前排名")
    private Integer ranking;

    /**
     * 市盈率
     */
    @Excel(name = "市盈率", orderNum = "16")
    @ApiModelProperty(value = "市盈率")
    private BigDecimal peRation;


}
