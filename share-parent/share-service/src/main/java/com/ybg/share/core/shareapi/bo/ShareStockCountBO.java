package com.ybg.share.core.shareapi.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class ShareStockCountBO implements Serializable {
    /**
     * 股票ID
     */
    @ApiModelProperty(value = "股票ID")
    private Long stockId;

    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String code;
    /**
     * 市场
     */
    @ApiModelProperty(value = "市场")
    private String market;
    /**
     * 上涨次数
     */
    @ApiModelProperty(value = "上涨次数")
    private Integer upTimes;
    /**
     * 下跌次数
     */
    @ApiModelProperty(value = "下跌次数")
    private Integer downTimes;
    /**
     * 最高价
     */
    @ApiModelProperty(value = "最高价")
    private BigDecimal topPrice;
    /**
     * 最低价
     */
    @ApiModelProperty(value = "最低价")
    private BigDecimal lowPrice;
    /**
     * 最高价日期
     */
    @ApiModelProperty(value = "最高价日期")
    private String topDate;
    /**
     * 最低价日期
     */
    @ApiModelProperty(value = "最低价日期")
    private String lowDate;

}
