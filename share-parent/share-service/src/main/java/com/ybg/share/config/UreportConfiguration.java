package com.ybg.share.config;

import com.bstek.ureport.console.UReportServlet;
import com.bstek.ureport.provider.report.ReportProvider;
import com.ybg.share.core.report.JDBCBuildinDataSource;
import com.ybg.share.core.report.MysqlReportProvider;
import com.ybg.share.core.report.RedisReportProvider;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.redis.core.RedisTemplate;

import javax.sql.DataSource;

/**
 * @author Deament
 */

@Configuration
@ImportResource("classpath:ureportcontext.xml")
@DependsOn("dataSource")
public class UreportConfiguration {

    @Bean(name = "uReportServlet")
    public ServletRegistrationBean buildUfloServlet() {
        return new ServletRegistrationBean(new UReportServlet(), "/ureport/*");
    }

    /**
     * 自定义的文件系统
     *
     * @return
     */
    //@Bean
    public ReportProvider redisReportProvider(RedisTemplate redisTemplate) {
        return new RedisReportProvider(redisTemplate);
    }

    /**
     * 自定义的文件系统
     *
     * @return
     */
    @Bean
    public ReportProvider mysqlReportProvider(DataSource dataSource) {
        return new MysqlReportProvider(dataSource);
    }


    @Bean(name="shareJDBCBuildinDataSource")
    public JDBCBuildinDataSource shareJDBCBuildinDataSource(DataSource dataSource){

        return  new JDBCBuildinDataSource(dataSource,"股票数据库");
    }
}
