package com.ybg.share.core.dbapi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.ybg.share.core.dbapi.dao.sql.ShareStockDayKSQL;
import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.remoteapi.dto.StockDayKDTO;
import com.ybg.share.core.remoteapi.vo.StockDayKVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 * 行情日k数据 Mapper 接口
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-21
 */
public interface ShareStockDayKMapper extends BaseMapper<ShareStockDayK> {

    @Insert("insert ignore into share_stock_day_k " +
            "(`ref_date`,`code`,`market`,`name`,`close_price`,`max_price`,`min_price`,`open_price`,`before_close`,`change_amount`," +
            "`change_range`,`turnover_rate`,`trade_num`,`trade_money`,`total_value`,`circulation_value`,`turnover_num`,`stock_id`) values " +
            "(#{refDate},#{code},#{market},#{name},#{closePrice},#{maxPrice},#{minPrice},#{openPrice},#{beforeClose},#{changeAmount}," +
            "#{changeRange},#{turnoverRate},#{tradeNum},#{tradeMoney},#{totalValue},#{circulationValue},#{turnoverNum},#{stockId})")
    boolean saveIgnore(ShareStockDayK shareStockDayK);

    @Select("SELECT ref_date FROM share_stock_day_k WHERE   stock_id=#{stockId} order by ref_date desc  limit 1")
    ShareStockDayK getMaxDateByCode(@Param("code") String code, @Param("stockId") Long stockId);

    @Select("SELECT id,`ref_date`,`code`,`market`,`name`,`close_price`,`max_price`,`min_price`,`open_price`,`before_close`,`change_amount`," +
            "`change_range`,`turnover_rate`,`trade_num`,`trade_money`,`total_value`,`circulation_value`,`turnover_num`,`stock_id` FROM share_stock_day_k WHERE   stock_id=#{stockId} order by ref_date desc  limit 1")
    ShareStockDayK getLastDayKByCode(@Param("code") String code, @Param("stockId") Long stockId);


    @UpdateProvider(type = ShareStockDayKSQL.class, method = "initTable")
    int initTable(@Param("mod") int mod);

    @SelectProvider(type = ShareStockDayKSQL.class, method = "listStockDayK")
    List<StockDayKVO> listStockDayK(@Param("dto") StockDayKDTO dto);

    @SelectProvider(type = ShareStockDayKSQL.class, method = "countStockDayK")
    Long countStockDayK(@Param("dto") StockDayKDTO dto);

}
