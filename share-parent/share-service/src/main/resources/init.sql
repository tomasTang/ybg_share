

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for share_financial_quarter
-- ----------------------------
-- IF EXISTS `share_financial_quarter`;
CREATE TABLE IF NOT EXISTS `share_financial_quarter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `report_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '报告月份 格式yyyy-MM-dd',
  `stock_id` bigint(20) NOT NULL COMMENT '个股ID',
  `code` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT '股票代码',
  `basic_eps` decimal(20,3) DEFAULT NULL COMMENT '基本每股收益(元)',
  `eps` decimal(20,3) DEFAULT NULL COMMENT '每股净资产(元)',
  `cash_flow` varchar(20) DEFAULT NULL COMMENT '每股经营活动产生的现金流量净额(元)',
  `revenue` decimal(20,3) DEFAULT NULL COMMENT '主营业务收入(万元)',
  `profitability` decimal(20,3) DEFAULT NULL COMMENT '主营业务利润(万元)',
  `operating_profit` decimal(20,3) DEFAULT NULL COMMENT '营业利润(万元)',
  `investment_return` decimal(20,3) DEFAULT NULL COMMENT '投资收益',
  `non_operating_income` decimal(20,3) DEFAULT NULL COMMENT '营业外收支净额(万元)',
  `total_profit` decimal(20,3) DEFAULT NULL COMMENT '利润总额(万元)',
  `net_profit` decimal(20,3) DEFAULT NULL COMMENT '净利润(万元)',
  `deduct_profit` decimal(20,3) DEFAULT NULL COMMENT '净利润(扣除非经常性损益后)(万元)',
  `net_cash_flow` decimal(20,3) DEFAULT NULL COMMENT '经营活动产生的现金流量净额(万元)',
  `net_increase_cash` decimal(20,3) DEFAULT NULL COMMENT '现金及现金等价物净增加额(万元)',
  `total_assets` decimal(20,3) DEFAULT NULL COMMENT '总资产(万元)',
  `current_assets` decimal(20,3) DEFAULT NULL COMMENT '流动资产(万元)',
  `total_liabilities` decimal(20,3) DEFAULT NULL COMMENT '总负债（万元）',
  `current_liabilities` decimal(20,3) DEFAULT NULL COMMENT '流动负债(万元)',
  `stockholder_equity` decimal(20,3) DEFAULT NULL COMMENT '股东权益不含少数股东权益(万元)',
  `weighted_return_equity` decimal(20,3) DEFAULT NULL COMMENT '净资产收益率加权(%)',
  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '交易市场',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201395 DEFAULT CHARSET=utf8mb4 COMMENT='财务报表-季度';

-- ----------------------------
-- Table structure for share_financial_year
-- ----------------------------
-- IF EXISTS `share_financial_year`;
CREATE TABLE IF NOT EXISTS `share_financial_year` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `report_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '报告年分 格式yyyy-MM-dd',
  `stock_id` bigint(20) NOT NULL COMMENT '个股ID',
  `code` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT '股票代码',
  `basic_eps` decimal(20,3) DEFAULT NULL COMMENT '基本每股收益(元)',
  `eps` decimal(20,3) DEFAULT NULL COMMENT '每股净资产(元)',
  `cash_flow` varchar(20) DEFAULT NULL COMMENT '每股经营活动产生的现金流量净额(元)',
  `revenue` decimal(20,3) DEFAULT NULL COMMENT '主营业务收入(万元)',
  `profitability` decimal(20,3) DEFAULT NULL COMMENT '主营业务利润(万元)',
  `operating_profit` decimal(20,3) DEFAULT NULL COMMENT '营业利润(万元)',
  `investment_return` decimal(20,3) DEFAULT NULL COMMENT '投资收益',
  `non_operating_income` decimal(20,3) DEFAULT NULL COMMENT '营业外收支净额(万元)',
  `total_profit` decimal(20,3) DEFAULT NULL COMMENT '利润总额(万元)',
  `net_profit` decimal(20,3) DEFAULT NULL COMMENT '净利润(万元)',
  `deduct_profit` decimal(20,3) DEFAULT NULL COMMENT '净利润(扣除非经常性损益后)(万元)',
  `net_cash_flow` decimal(20,3) DEFAULT NULL COMMENT '经营活动产生的现金流量净额(万元)',
  `net_increase_cash` decimal(20,3) DEFAULT NULL COMMENT '现金及现金等价物净增加额(万元)',
  `total_assets` decimal(20,3) DEFAULT NULL COMMENT '总资产(万元)',
  `current_assets` decimal(20,3) DEFAULT NULL COMMENT '流动资产(万元)',
  `total_liabilities` decimal(20,3) DEFAULT NULL COMMENT '总负债（万元）',
  `current_liabilities` decimal(20,3) DEFAULT NULL COMMENT '流动负债(万元)',
  `stockholder_equity` decimal(20,3) DEFAULT NULL COMMENT '股东权益不含少数股东权益(万元)',
  `weighted_return_equity` decimal(20,3) DEFAULT NULL COMMENT '净资产收益率加权(%)',
  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '交易市场',
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_date` (`report_date`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=60876 DEFAULT CHARSET=utf8mb4 COMMENT='财务报表 年度';

-- ----------------------------
-- Table structure for share_fix_dayk_task
-- ----------------------------
-- IF EXISTS `share_fix_dayk_task`;
CREATE TABLE IF NOT EXISTS `share_fix_dayk_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `time` int(11) NOT NULL DEFAULT '0' COMMENT '第几次修复，默认0',
  `update_time` datetime DEFAULT NULL COMMENT '最后一次更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stock_id` (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5279 DEFAULT CHARSET=utf8mb4 COMMENT='日K自我修复任务';

-- ----------------------------
-- Table structure for share_receive_email
-- ----------------------------
-- IF EXISTS `share_receive_email`;
CREATE TABLE IF NOT EXISTS `share_receive_email` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '邮箱',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `start_time` datetime DEFAULT NULL COMMENT '服务起始时间',
  `end_time` datetime DEFAULT NULL COMMENT '服务结束时间',
  PRIMARY KEY (`id`),
  KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COMMENT='会员接收邮箱的列表';

-- ----------------------------
-- Table structure for share_stock
-- ----------------------------
-- IF EXISTS `share_stock`;
CREATE TABLE IF NOT EXISTS `share_stock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(60) NOT NULL COMMENT '股票名称',
  `code` varchar(10) NOT NULL COMMENT '1 可用，0禁用',
  `market` varchar(10) NOT NULL COMMENT '所属市场',
  `status` tinyint(1) DEFAULT '1' COMMENT '1 可用 0不可用',
  `spell` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票拼音',
  `cindustry` varchar(60) DEFAULT NULL COMMENT '所属行业',
  `company_name` varchar(255) DEFAULT NULL COMMENT '企业名字',
  `registered_capital` decimal(10,2) DEFAULT NULL COMMENT '注册资本（万元）',
  `tel` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT '公司电话',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `name` (`name`),
  KEY `spell` (`spell`)
) ENGINE=InnoDB AUTO_INCREMENT=71548 DEFAULT CHARSET=utf8mb4 COMMENT='股票信息';

-- ----------------------------
-- Table structure for share_stock_count_month
-- ----------------------------
-- IF EXISTS `share_stock_count_month`;
CREATE TABLE IF NOT EXISTS `share_stock_count_month` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `ref_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '日期',
  `code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场',
  `up_times` int(3) DEFAULT NULL COMMENT '上涨次数',
  `down_times` int(3) DEFAULT NULL COMMENT '下跌次数',
  `top_price` decimal(10,2) DEFAULT NULL COMMENT '最高价',
  `low_price` decimal(10,2) DEFAULT NULL COMMENT '最低价',
  `top_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最高价日期',
  `low_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最低价日期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_index` (`stock_id`,`ref_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='个股月度统计';

-- ----------------------------
-- Table structure for share_stock_count_month0
-- ----------------------------
-- IF EXISTS `share_stock_count_month0`;
CREATE TABLE IF NOT EXISTS `share_stock_count_month0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `ref_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '日期',
  `code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场',
  `up_times` int(3) DEFAULT NULL COMMENT '上涨次数',
  `down_times` int(3) DEFAULT NULL COMMENT '下跌次数',
  `top_price` decimal(10,2) DEFAULT NULL COMMENT '最高价',
  `low_price` decimal(10,2) DEFAULT NULL COMMENT '最低价',
  `top_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最高价日期',
  `low_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最低价日期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_index` (`stock_id`,`ref_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7111 DEFAULT CHARSET=utf8mb4 COMMENT='个股月度统计';

-- ----------------------------
-- Table structure for share_stock_count_month1
-- ----------------------------
-- IF EXISTS `share_stock_count_month1`;
CREATE TABLE IF NOT EXISTS `share_stock_count_month1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `ref_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '日期',
  `code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场',
  `up_times` int(3) DEFAULT NULL COMMENT '上涨次数',
  `down_times` int(3) DEFAULT NULL COMMENT '下跌次数',
  `top_price` decimal(10,2) DEFAULT NULL COMMENT '最高价',
  `low_price` decimal(10,2) DEFAULT NULL COMMENT '最低价',
  `top_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最高价日期',
  `low_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最低价日期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_index` (`stock_id`,`ref_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7199 DEFAULT CHARSET=utf8mb4 COMMENT='个股月度统计';

-- ----------------------------
-- Table structure for share_stock_count_month2
-- ----------------------------
-- IF EXISTS `share_stock_count_month2`;
CREATE TABLE IF NOT EXISTS `share_stock_count_month2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `ref_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '日期',
  `code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场',
  `up_times` int(3) DEFAULT NULL COMMENT '上涨次数',
  `down_times` int(3) DEFAULT NULL COMMENT '下跌次数',
  `top_price` decimal(10,2) DEFAULT NULL COMMENT '最高价',
  `low_price` decimal(10,2) DEFAULT NULL COMMENT '最低价',
  `top_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最高价日期',
  `low_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最低价日期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_index` (`stock_id`,`ref_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7187 DEFAULT CHARSET=utf8mb4 COMMENT='个股月度统计';

-- ----------------------------
-- Table structure for share_stock_count_month3
-- ----------------------------
-- IF EXISTS `share_stock_count_month3`;
CREATE TABLE IF NOT EXISTS `share_stock_count_month3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `ref_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '日期',
  `code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场',
  `up_times` int(3) DEFAULT NULL COMMENT '上涨次数',
  `down_times` int(3) DEFAULT NULL COMMENT '下跌次数',
  `top_price` decimal(10,2) DEFAULT NULL COMMENT '最高价',
  `low_price` decimal(10,2) DEFAULT NULL COMMENT '最低价',
  `top_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最高价日期',
  `low_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最低价日期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_index` (`stock_id`,`ref_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7868 DEFAULT CHARSET=utf8mb4 COMMENT='个股月度统计';

-- ----------------------------
-- Table structure for share_stock_count_month4
-- ----------------------------
-- IF EXISTS `share_stock_count_month4`;
CREATE TABLE IF NOT EXISTS `share_stock_count_month4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `ref_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '日期',
  `code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场',
  `up_times` int(3) DEFAULT NULL COMMENT '上涨次数',
  `down_times` int(3) DEFAULT NULL COMMENT '下跌次数',
  `top_price` decimal(10,2) DEFAULT NULL COMMENT '最高价',
  `low_price` decimal(10,2) DEFAULT NULL COMMENT '最低价',
  `top_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最高价日期',
  `low_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最低价日期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_index` (`stock_id`,`ref_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6620 DEFAULT CHARSET=utf8mb4 COMMENT='个股月度统计';

-- ----------------------------
-- Table structure for share_stock_count_month5
-- ----------------------------
-- IF EXISTS `share_stock_count_month5`;
CREATE TABLE IF NOT EXISTS `share_stock_count_month5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `ref_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '日期',
  `code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场',
  `up_times` int(3) DEFAULT NULL COMMENT '上涨次数',
  `down_times` int(3) DEFAULT NULL COMMENT '下跌次数',
  `top_price` decimal(10,2) DEFAULT NULL COMMENT '最高价',
  `low_price` decimal(10,2) DEFAULT NULL COMMENT '最低价',
  `top_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最高价日期',
  `low_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最低价日期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_index` (`stock_id`,`ref_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='个股月度统计';

-- ----------------------------
-- Table structure for share_stock_count_year
-- ----------------------------
-- IF EXISTS `share_stock_count_year`;
CREATE TABLE IF NOT EXISTS `share_stock_count_year` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `ref_date` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '日期',
  `code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
  `market` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场',
  `up_times` int(3) DEFAULT NULL COMMENT '上涨次数',
  `down_times` int(3) DEFAULT NULL COMMENT '下跌次数',
  `top_price` decimal(10,2) DEFAULT NULL COMMENT '最高价',
  `low_price` decimal(10,2) DEFAULT NULL COMMENT '最低价',
  `top_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最高价日期',
  `low_date` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '最低价日期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_index` (`stock_id`,`ref_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=460 DEFAULT CHARSET=utf8mb4 COMMENT='个股年度统计';

-- ----------------------------
-- Table structure for share_stock_day_k
-- ----------------------------
-- IF EXISTS `share_stock_day_k`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k0
-- ----------------------------
-- IF EXISTS `share_stock_day_k0`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k0` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k0` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1372008 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k1
-- ----------------------------
-- IF EXISTS `share_stock_day_k1`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k1` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k1` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1371122 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k10
-- ----------------------------
-- IF EXISTS `share_stock_day_k10`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k10` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k10` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k10` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1365262 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k11
-- ----------------------------
-- IF EXISTS `share_stock_day_k11`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k11` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k11` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k11` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1372774 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k12
-- ----------------------------
-- IF EXISTS `share_stock_day_k12`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k12` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k12` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k12` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1324561 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k13
-- ----------------------------
-- IF EXISTS `share_stock_day_k13`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k13` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k13` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k13` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1343242 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k14
-- ----------------------------
-- IF EXISTS `share_stock_day_k14`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k14` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k14` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k14` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1263868 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k15
-- ----------------------------
-- IF EXISTS `share_stock_day_k15`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k15` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k15` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k15` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1352048 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k16
-- ----------------------------
-- IF EXISTS `share_stock_day_k16`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k16` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k16` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k16` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1353056 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k17
-- ----------------------------
-- IF EXISTS `share_stock_day_k17`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k17` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k17` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k17` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1344568 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k18
-- ----------------------------
-- IF EXISTS `share_stock_day_k18`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k18` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k18` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k18` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1309033 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k19
-- ----------------------------
-- IF EXISTS `share_stock_day_k19`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k19` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k19` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1307049 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k2
-- ----------------------------
-- IF EXISTS `share_stock_day_k2`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k2` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1389919 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k20
-- ----------------------------
-- IF EXISTS `share_stock_day_k20`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k20` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k20` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k20` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=755013 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k21
-- ----------------------------
-- IF EXISTS `share_stock_day_k21`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k21` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=759588 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k22
-- ----------------------------
-- IF EXISTS `share_stock_day_k22`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k22` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=723986 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k23
-- ----------------------------
-- IF EXISTS `share_stock_day_k23`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k23` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=723928 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k24
-- ----------------------------
-- IF EXISTS `share_stock_day_k24`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k24` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=727337 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k25
-- ----------------------------
-- IF EXISTS `share_stock_day_k25`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k25` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=711733 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k26
-- ----------------------------
-- IF EXISTS `share_stock_day_k26`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k26` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=734680 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k27
-- ----------------------------
-- IF EXISTS `share_stock_day_k27`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k27` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=731555 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k28
-- ----------------------------
-- IF EXISTS `share_stock_day_k28`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k28` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=689066 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k29
-- ----------------------------
-- IF EXISTS `share_stock_day_k29`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k29` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=737706 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k3
-- ----------------------------
-- IF EXISTS `share_stock_day_k3`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k3` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k3` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1394116 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k30
-- ----------------------------
-- IF EXISTS `share_stock_day_k30`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k30` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=752253 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k31
-- ----------------------------
-- IF EXISTS `share_stock_day_k31`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k31` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=723730 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k32
-- ----------------------------
-- IF EXISTS `share_stock_day_k32`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k32` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=688968 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k33
-- ----------------------------
-- IF EXISTS `share_stock_day_k33`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k33` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=708889 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k34
-- ----------------------------
-- IF EXISTS `share_stock_day_k34`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k34` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=729612 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k35
-- ----------------------------
-- IF EXISTS `share_stock_day_k35`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k35` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=714192 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k36
-- ----------------------------
-- IF EXISTS `share_stock_day_k36`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k36` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=724289 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k37
-- ----------------------------
-- IF EXISTS `share_stock_day_k37`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k37` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=730779 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k38
-- ----------------------------
-- IF EXISTS `share_stock_day_k38`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k38` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=714227 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k39
-- ----------------------------
-- IF EXISTS `share_stock_day_k39`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k39` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=690447 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k4
-- ----------------------------
-- IF EXISTS `share_stock_day_k4`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k4` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k4` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1308596 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k40
-- ----------------------------
-- IF EXISTS `share_stock_day_k40`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k40` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k19` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k5
-- ----------------------------
-- IF EXISTS `share_stock_day_k5`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k5` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k5` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1312846 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k6
-- ----------------------------
-- IF EXISTS `share_stock_day_k6`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k6` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k6` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1329532 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k7
-- ----------------------------
-- IF EXISTS `share_stock_day_k7`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k7` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k7` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1346573 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k8
-- ----------------------------
-- IF EXISTS `share_stock_day_k8`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k8` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k8` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k8` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1316714 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_day_k9
-- ----------------------------
-- IF EXISTS `share_stock_day_k9`;
CREATE TABLE IF NOT EXISTS `share_stock_day_k9` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `turnover_rate` decimal(10,4) DEFAULT NULL COMMENT '换手率',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  `total_value` bigint(20) DEFAULT NULL COMMENT '总市值',
  `circulation_value` bigint(20) DEFAULT NULL COMMENT '流通市值',
  `turnover_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_2_share_stock_day_k9` (`ref_date`,`code`,`market`),
  KEY `date_share_stock_day_k9` (`ref_date`,`code`,`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1380428 DEFAULT CHARSET=utf8mb4 COMMENT='行情日k数据';

-- ----------------------------
-- Table structure for share_stock_email_data
-- ----------------------------
-- IF EXISTS `share_stock_email_data`;
CREATE TABLE IF NOT EXISTS `share_stock_email_data` (
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  `ref_year` int(11) DEFAULT NULL COMMENT '年份',
  `buy_price` decimal(10,2) DEFAULT NULL COMMENT '买入价格',
  `sell_price` decimal(10,2) DEFAULT NULL COMMENT '卖出价格',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `times` int(11) DEFAULT NULL COMMENT '最大次数',
  `profit` decimal(10,2) DEFAULT NULL COMMENT '盈利',
  `profit_rate` decimal(10,2) DEFAULT NULL COMMENT '利润率',
  `stock_code` varchar(8) DEFAULT NULL COMMENT '股票代码',
  `market` varchar(2) DEFAULT NULL COMMENT '市场',
  `stock_name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `price` decimal(10,2) DEFAULT NULL COMMENT '当前价，取自前一个日K收盘价',
  `odds_ratio` decimal(10,4) DEFAULT NULL COMMENT '差异比,数字越大则利润率越小',
  `focus` decimal(10,2) DEFAULT NULL COMMENT '关注指数',
  `jgcyd` decimal(10,2) DEFAULT NULL COMMENT '机构参与度',
  `jgcyd_type` varchar(10) DEFAULT NULL COMMENT '机构参与度描述',
  `total_score` decimal(10,2) DEFAULT NULL COMMENT '总和得分',
  `ranking` int(11) DEFAULT NULL COMMENT '当前排名',
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pe_ration` decimal(10,2) DEFAULT NULL COMMENT '市盈率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101753 DEFAULT CHARSET=utf8mb4 COMMENT='email 全量数据';

-- ----------------------------
-- Table structure for share_stock_email_data_history
-- ----------------------------
-- IF EXISTS `share_stock_email_data_history`;
CREATE TABLE IF NOT EXISTS `share_stock_email_data_history` (
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  `ref_year` int(11) DEFAULT NULL COMMENT '年份',
  `buy_price` decimal(10,2) DEFAULT NULL COMMENT '买入价格',
  `sell_price` decimal(10,2) DEFAULT NULL COMMENT '卖出价格',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `times` int(11) DEFAULT NULL COMMENT '最大次数',
  `profit` decimal(10,2) DEFAULT NULL COMMENT '盈利',
  `profit_rate` decimal(10,2) DEFAULT NULL COMMENT '利润率',
  `stock_code` varchar(8) DEFAULT NULL COMMENT '股票代码',
  `market` varchar(2) DEFAULT NULL COMMENT '市场',
  `stock_name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `price` decimal(10,2) DEFAULT NULL COMMENT '当前价，取自前一个日K收盘价',
  `odds_ratio` decimal(10,4) DEFAULT NULL COMMENT '差异比,数字越大则利润率越小',
  `focus` decimal(10,2) DEFAULT NULL COMMENT '关注指数',
  `jgcyd` decimal(10,2) DEFAULT NULL COMMENT '机构参与度',
  `jgcyd_type` varchar(10) DEFAULT NULL COMMENT '机构参与度描述',
  `total_score` decimal(10,2) DEFAULT NULL COMMENT '总和得分',
  `ranking` int(11) DEFAULT NULL COMMENT '当前排名',
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pe_ration` decimal(10,2) DEFAULT NULL COMMENT '市盈率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100490 DEFAULT CHARSET=utf8mb4 COMMENT='email 全量数据历史';

-- ----------------------------
-- Table structure for share_stock_evaluate
-- ----------------------------
-- IF EXISTS `share_stock_evaluate`;
CREATE TABLE IF NOT EXISTS `share_stock_evaluate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '股票ID',
  `stock_code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
  `stock_name` varchar(60) DEFAULT NULL COMMENT '股票名称',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后一次更新时间',
  `change_percent` decimal(8,2) DEFAULT NULL COMMENT '涨跌幅',
  `focus` decimal(10,0) DEFAULT NULL COMMENT '关注指数',
  `jgcyd` decimal(10,2) DEFAULT NULL COMMENT '机构参与度',
  `jgcyd_type` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '机构参与度描述',
  `now_price` decimal(10,2) DEFAULT NULL COMMENT '最新价',
  `ranking_up` int(11) DEFAULT NULL COMMENT '排名上升',
  `total_score` decimal(10,2) DEFAULT NULL COMMENT '综合得分',
  `turnover_rate` decimal(10,2) DEFAULT NULL COMMENT '换手率',
  `zlcb` decimal(10,2) DEFAULT NULL COMMENT '主力成本',
  `zlcb20r` decimal(10,2) DEFAULT NULL COMMENT '主力成本20日',
  `zlcb60r` decimal(10,2) DEFAULT NULL COMMENT '主力成本六十日',
  `zljlr` decimal(15,0) DEFAULT NULL COMMENT '主力净流入',
  `ranking` int(11) DEFAULT NULL COMMENT '当前排名',
  `pe_ration` decimal(11,2) DEFAULT NULL COMMENT '市盈率',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stock_code` (`stock_code`)
) ENGINE=InnoDB AUTO_INCREMENT=186820 DEFAULT CHARSET=utf8mb4 COMMENT='千股千评';

-- ----------------------------
-- Table structure for share_stock_gravity
-- ----------------------------
-- IF EXISTS `share_stock_gravity`;
CREATE TABLE IF NOT EXISTS `share_stock_gravity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `stock_name` varchar(60) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票名称',
  `stock_code` varchar(19) CHARACTER SET utf8 NOT NULL COMMENT '股票代码',
  `now_price` decimal(10,3) DEFAULT NULL COMMENT '当前价',
  `line_price` decimal(10,3) DEFAULT NULL COMMENT '引力线价格',
  `line_price110` decimal(10,3) DEFAULT NULL COMMENT '引力线价格*1.1',
  `line_price120` decimal(10,3) DEFAULT NULL COMMENT '引力线价格*1.2',
  `line_price90` decimal(10,3) DEFAULT NULL COMMENT '引力线价格*0.9',
  `line_price80` decimal(10,3) DEFAULT NULL COMMENT '引力线价格*0.8',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stock_id` (`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3873 DEFAULT CHARSET=utf8mb4 COMMENT='地心引力检测表';

-- ----------------------------
-- Table structure for share_stock_index
-- ----------------------------
-- IF EXISTS `share_stock_index`;
CREATE TABLE IF NOT EXISTS `share_stock_index` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ref_date` varchar(20) DEFAULT NULL COMMENT '日期，2019-10-16',
  `code` varchar(20) DEFAULT NULL COMMENT '股票代码',
  `market` varchar(20) DEFAULT NULL COMMENT '所属市场',
  `stock_name` varchar(60) DEFAULT NULL COMMENT '名称',
  `close_price` decimal(10,4) DEFAULT NULL COMMENT '收盘价',
  `max_price` decimal(10,4) DEFAULT NULL COMMENT '最高价',
  `min_price` decimal(10,4) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10,4) DEFAULT NULL COMMENT '开盘价',
  `before_close` decimal(10,4) DEFAULT NULL COMMENT '前收盘',
  `change_amount` decimal(10,4) DEFAULT NULL COMMENT '涨跌额',
  `change_range` decimal(10,4) DEFAULT NULL COMMENT '涨跌幅',
  `trade_num` bigint(20) DEFAULT NULL COMMENT '成交量',
  `trade_money` decimal(20,4) DEFAULT NULL COMMENT '成交金额',
  PRIMARY KEY (`id`),
  UNIQUE KEY `only` (`ref_date`,`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14003 DEFAULT CHARSET=utf8mb4 COMMENT='上证指数和深证指数';

-- ----------------------------
-- Table structure for share_stock_pbest
-- ----------------------------
-- IF EXISTS `share_stock_pbest`;
CREATE TABLE IF NOT EXISTS `share_stock_pbest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `stock_id` bigint(20) NOT NULL COMMENT '股票ID',
  `ref_year` int(5) NOT NULL COMMENT '年份',
  `buy_price` decimal(10,2) NOT NULL COMMENT '买入价格',
  `sell_price` decimal(10,2) NOT NULL COMMENT '卖出价格',
  `pbest_name` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '算法名称',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `times` int(11) DEFAULT '0' COMMENT '最大次数',
  `profit` decimal(10,2) DEFAULT NULL COMMENT '盈利',
  `profit_rate` decimal(10,4) DEFAULT NULL COMMENT '利润率',
  `stock_code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票代码',
  `market` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '市场',
  `stock_name` varchar(60) CHARACTER SET utf8 DEFAULT NULL COMMENT '股票名称',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shock_id` (`stock_id`,`ref_year`,`pbest_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=185199 DEFAULT CHARSET=utf8mb4 COMMENT='最佳买卖算法记录表';

-- ----------------------------
-- Table structure for share_stock_pbest_history
-- ----------------------------
-- IF EXISTS `share_stock_pbest_history`;
CREATE TABLE IF NOT EXISTS `share_stock_pbest_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT ' 主键 ',
  `shock_id` bigint(20) NOT NULL COMMENT ' 股票ID ',
  `ref_year` int(5) NOT NULL COMMENT ' 年份 ',
  `buy_price` decimal(10,2) NOT NULL COMMENT ' 买入价格 ',
  `sell_price` decimal(10,2) NOT NULL COMMENT ' 卖出价格 ',
  `pbest_name` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT ' 算法名称 ',
  `update_time` datetime DEFAULT NULL COMMENT ' 更新时间 ',
  `create_time` datetime DEFAULT NULL COMMENT ' 创建时间 ',
  `times` int(11) DEFAULT '0' COMMENT ' 最大次数 ',
  `profit` decimal(10,2) DEFAULT NULL COMMENT ' 盈利 ',
  `profit_rate` decimal(10,4) DEFAULT NULL COMMENT ' 利润率 ',
  `shock_code` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT ' 股票代码 ',
  `market` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT ' 市场 ',
  `shock_name` varchar(60) CHARACTER SET utf8 DEFAULT NULL COMMENT ' 股票名称 ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shock_id` (`shock_id`,`ref_year`,`pbest_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='最佳买卖算法记录表(历史 非当前年份)';
