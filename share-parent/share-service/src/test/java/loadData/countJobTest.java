package loadData;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.ShareApplication;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.pbest.service.Day250PbestService;
import com.ybg.share.core.remoteapi.StockRemoteApi;
import com.ybg.share.core.shareapi.impl.MonthCountStockApi;
import com.ybg.share.core.shareapi.impl.YearCountStockApi;
import com.ybg.share.utils.ThreadUtil;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@SpringBootTest(classes = ShareApplication.class)
@RunWith(SpringRunner.class)
public class countJobTest {

    @Autowired
    YearCountStockApi yearCountStockApi;
    @Autowired
    ShareStockService shareStockService;

    @Test
    public void testyear() {

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ShareStock.STATUS, 1);
        List<ShareStock> list = shareStockService.list(queryWrapper);
        int year = 2018;

        for (ShareStock shareStock : list) {
            yearCountStockApi.execute(shareStock, year + "");
        }
//        ShareStock shareStock = shareStockService.getById(1);
//        yearCountStockApi.execute(shareStock,"2018");
    }

    @Autowired
    MonthCountStockApi monthCountStockApi;

    @Test
    public void testmonth() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ShareStock.STATUS, 1);
        queryWrapper.ge(ShareStock.ID, 147);
        List<ShareStock> list = shareStockService.list(queryWrapper);
        int year = 2018;
        String months[] = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        for (ShareStock shareStock : list) {
            System.out.println(shareStock.toString());
            for (int i = 1991; i < 2019; i++) {
                for (String month : months) {
                    monthCountStockApi.execute(shareStock, i + "-" + month);
                }
            }
        }
//        ShareStock shareStock = shareStockService.getById(1);
//        yearCountStockApi.execute(shareStock,"2018");
    }

    @Autowired
    StockRemoteApi stockRemoteApi;

    @Test
    public void testThread() {
        stockRemoteApi.syncAllStock();
        while (true) {
            try {
                System.out.println("等待数" + ThreadUtil.THREAD_SEMAPHORE.getQueueLength());
                System.out.println("可用队列" + ThreadUtil.THREAD_SEMAPHORE.availablePermits());
                Thread.sleep(30000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Autowired
    Day250PbestService day250PbestService;

    @Test
    public void testLoad250() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ShareStock.STATUS, 1);
        List<ShareStock> list = shareStockService.list(queryWrapper);
        int year = DateUtil.year(new Date());
        for (ShareStock shareStock : list) {
            //pbestService.simplePbest(shareStock, year);
            day250PbestService.dat250Pbest(shareStock);
        }
    }

}
