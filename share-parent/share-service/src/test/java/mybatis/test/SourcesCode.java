package mybatis.test;

import org.apache.ibatis.session.*;

import java.sql.Connection;

public class SourcesCode {
    public static void main(String[] args) {
        SqlSessionFactory factory=new SqlSessionFactory() {
            @Override
            public SqlSession openSession() {
                return null;
            }

            @Override
            public SqlSession openSession(boolean b) {
                return null;
            }

            @Override
            public SqlSession openSession(Connection connection) {
                return null;
            }

            @Override
            public SqlSession openSession(TransactionIsolationLevel transactionIsolationLevel) {
                return null;
            }

            @Override
            public SqlSession openSession(ExecutorType executorType) {
                return null;
            }

            @Override
            public SqlSession openSession(ExecutorType executorType, boolean b) {
                return null;
            }

            @Override
            public SqlSession openSession(ExecutorType executorType, TransactionIsolationLevel transactionIsolationLevel) {
                return null;
            }

            @Override
            public SqlSession openSession(ExecutorType executorType, Connection connection) {
                return null;
            }

            @Override
            public Configuration getConfiguration() {
                return null;
            }
        };
        SqlSession sqlSession = factory.openSession();
        sqlSession.selectOne("");

    }
}
