#股票模块
启动 share-service 即可 不需要启动 share-admin 模块
启动之前需要启动redis ，mysql
修改share-service 的配置文件
把 application.properties 的内容spring.profiles.active=pro 改成 spring.profiles.active=dev
并且修改好你的配置文件
application-dev.properties

### 必须要注意的是，reids 的配置，mysql的配置,还有你的网易邮箱配置
```
#mysql 使用的版本是5.7+
sharding.spring.datasource.url = jdbc:mysql://127.0.0.1:3306/ybg_share?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&useSSL=true&allowMultiQueries=true&autoReconnect=true&failOverReadOnly=false
sharding.spring.datasource.username = root
sharding.spring.datasource.password = 123456

#redis的配置，不推荐使用windows的redis 会产生假死的问题
spring.redis.database=3

spring.redis.host=127.0.0.1
spring.redis.port=6379
spring.redis.password=

#网易邮箱配置 如果不填，发送邮箱的定时任务将无法进行
email.netaset.account=
email.netaset.password=

```


#定时任务列表

 ## 0点到1点

 ## 1点到2点
 ## 2点到3点
 - 拉取个股列表 0 30 2 * * ?
 ## 3点到4点
 - 更新大盘 0 30 3 * * ?

 ## 4点到5点
  - 拉取日K 0 30 4 * * ?
 ## 5点到6点
  - 地心引力线定时任务 0 30 5 * * ?
 ## 6点到7点
 - Redis定时任务
   - 最新日K放到redis中 0 30 6 * * ?
  
 ## 7点到8点
 ## 8点到9点
 - 财报数据定时任务
   - 更新季度财报数据  1 30 8 1 1/1 ?
   
 ## 9点到10点
 - 财报净利润放到redis中 0 30 10 * * ?
 ## 10点到11点
 ## 11点到12点
 ## 12点到13点
 ## 13点到14点
 ## 14点到15点
 ## 15点到16点
 ## 16点到17点
 ## 17点到18点
 ## 18点到19点
 ## 19点到20点
 ## 20点到21点
 - 定时定量修复日K数据 0 30 20 * * ?
 ## 21点到22点
 - 千股千评定时任务
    - 千股千评 数据拉取 0 30 21 * * ?
 ## 22点到23点
 ## 23点到00点
 
       