package com.ybg.framework.dto;

/**
 * 抽象任务
 */

public abstract class AbstractJob {

    Object params;

    public Object getParams() {
        return params;
    }

    public void setParams(Object params) {
        this.params = params;
    }

    public AbstractJob(Object params) {
        setParams(params);
    }

    public abstract void execute();

}
