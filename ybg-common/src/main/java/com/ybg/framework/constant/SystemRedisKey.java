package com.ybg.framework.constant;

/**
 * 系统配置redis键
 */
public interface SystemRedisKey {
    /**
     * 同步K先开始时间策略,
     */
    String SYNC_DAY_K_POLICY="SYNC_DAY_K_POLICY";

    /**
     * 是否完成初始化
     */
    String INIT_TABLE_FINISH="initTableFinish";
}
